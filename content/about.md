---
title: "About"
layout: "about"
image: "images/about.png"
draft: false

#caption
caption:
  enable: true
  title: "Connect | Collaborate | Contribute"

# social
social:
  enable: true
  social_links:
  - link: "mailto:sotc@freelists.org"
    icon: "lar la-envelope"

  - link: "https://signal.group/#CjQKIC_ZRipL_Kqnp2py2k0KovMEYHNbhWi2g77Lb7gUktfZEhA688UWP-Sx7FxepNdtUC9-"
    icon: "lar la-comments"

  - link: "https://www.gitlab.com/sotc/"
    icon: "lab la-gitlab"
  
  - link: "https://www.instagram.com/sotcpu/"
    icon: "lab la-instagram"


# what_i_do
what_i_do:
  title: "What We Do"
  enable: false
  item:
  - title: "Learn"
    icon: "las la-pen-nib"
    description: "Purus eget ipsum elementum venenatis, quis rutrum mi semper nonpurus eget ipsum elementum venenatis."
  
  - title: "Share"
    icon: "las la-chalkboard-teacher"
    description: "Aenean maximus urna magna elementum venenatis, quis rutrum mi semper non purus eget ipsum elementum venenatis."
  
  - title: "Collaborate"
    icon: "las la-users"
    description: "Aenean maximus urna magna elementum venenatis, quis rutrum mi semper non purus eget ipsum elementum venenatis."
---
Throughout the history, technologies from the bottom up has always helped human race to progress forward. But these technologies has become a tool of oppression for the mighty people. `SOTC` is a community formed by `The Students of Pondicherry University` to discuss and respond to these issues, with a motto of making technology `open` and `inclusive`. This community strives to bring people from every strata of the society and all the quarters of the country, to generate more inclusive and dynamic ideas.<br /><br />A space like Pondicherry University campus is a boon for this community to bring young minds together from various academic discipline as well as from different parts of the country. Ever since the community was formed it has organized several talks, discussions and different events on areas like knowledge commons includes open access to journals, privacy, data ownership, surveillance, localization, decentralization and Free & Open Source Software(FOSS) with its philosophies. The community work towards the new challenges in technological landscape, narrowing down the prevailing digital divide, and to make technology more consensual to the masses.

Join the [Signal group](https://signal.group/#CjQKIC_ZRipL_Kqnp2py2k0KovMEYHNbhWi2g77Lb7gUktfZEhA688UWP-Sx7FxepNdtUC9-) of SOTC for more updates!!!


![Signal group qr code](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/sotc-signal-group.png)
