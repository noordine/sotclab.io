---
title: "A small review on WEBP format"
image: "images/post/07.webp"
date: 2024-04-13
author: "Sridhar"
categories: ["foss"]
tags: ["foss", "ffmpeg","webp"]
draft: false
---
## About .webp file format

A quick intro about webp
* WebP is a raster graphics file format intended as a replacement for JPEG, PNG, and GIF file formats.

I have been toying around with [WebP](https://en.wikipedia.org/wiki/WebP) for a couple of weeks now. It was pretty crazy for me to see an up to 50% image size reduction, yet very little to no visual difference with a naked human eye.

Some further reading led me to some pleasant discoveries, such as lossy and lossless compression, metadata, quality metrics, support, and a wide adoption by major browsers.

I'm sharing all my enthusiasm authentically and this led me to discover some more free software tools like ffmpeg, imagemagick and pngcrush. All of these tools have Command-line processing which i personally used and tested.

## The Conversion

---

#### About ffmpeg

- The [libavcodec](https://en.wikipedia.org/wiki/Libavcodec) library from the [FFmpeg](https://en.wikipedia.org/wiki/FFmpeg) project provides many of [VLC's](https://en.wikipedia.org/wiki/VLC_media_player) codecs

![screenshot](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/p07_01.webp)

- Not only images, it also converts video formats from one format to another. For example, I converted .mov file which my friend took a video during diwali in his iPhone, this format cannot be viewed in my mobile(andriod), so this tool converted this 34.7 MiB .mov file into .mp4 within a matter of seconds(prolly 10sec maybe), also this has [advanced options](https://ffmpeg.org/ffmpeg.html#Advanced-Video-options) to customize the video based on our needs.

![screenshot](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/p07_02.webp)

- This instigated to write a python script which is available in my [gitlab repo](https://gitlab.com/SridharRG/file-format-conversion) for converting a folder of .jpg files into .webp

```python
import os

dir = os.listdir("/home/sridhar/Documents/FSHM/workshop-pics")

for i in dir:
	if i.endswith(".jpg"):
		cmd = "ffmpeg -i "  + str(i) + " " + str(i) +".webp"
		os.system(cmd)
```

* This Script took 1min 18sec to convert 65 .jpg files each ranging 2.5mb to .webp format

![bunchss](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/p07_03.webp)

#### About pngcrush

- this also does the same with cli, converting one format of images into another format. The process of moving from PNG to WebP was painless.
- Below is the pngcrush version

```sh
pngcrush --version
Warning: versions are different between png.h and png.c
  png.h version: 1.6.37
  png.c version: 1.6.43

 pngcrush 1.8.13, uses libpng 1.6.37 and zlib 1.2.11
 Check http://pmt.sf.net/
 for the most recent version.
```

- But personally won't recommend using pngcrush because it takes 49 seconds to convert a 10.7mb .png file to .webp file, whereas ffmpeg and ImageMagick does the job in matter of seconds in the same case, also It doesn't compress the the image and only convert the format.

```sh
❯ pngcrush ign_unsplash4.png pngcrush.webp
Warning: versions are different between png.h and png.c
  png.h version: 1.6.37
  png.c version: 1.6.43

  Recompressing IDAT chunks in ign_unsplash4.png to pngcrush.webp
   Total length of data found in critical chunks            =  11208045
   Best pngcrush method        =   7 (ws 15 fm 0 zl 9 zs 0) =   5811470
CPU time decode 1.085696, encode 47.969723, other 0.017002, total 49.087856 sec
```

## Example ?

---

Let’s try to grab a picture in PNG format, convert it to WebP and see how the file size and image quality differ between those two.

**Here is a wallpaper in PNG, weighing at around 2.5MB**
![PNG, weighing at around 2.5MB](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/wallpaper.jpg)

**As a comparison, here is the WebP generated from it, which is only about 463.8KiB**
![As a comparison, WebP 463.8KiB](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/ffmpeg.webp)

WebP is good for all the stuff on the web. Choose your image formats based on your needs and requirements. For now, WebP looks like a great alternative. Thank you!


For further readings, refer
1. [Speed up your website -webp](https://www.smashingmagazine.com/2019/10/speed-up-your-website-webp/#webp-advantages)
2. [lossless and transparency encoding](https://www.ctrl.blog/entry/webp-flif-comparison.html)
3. [Is webp really better than jpeg](https://siipo.la/blog/is-webp-really-better-than-jpeg)