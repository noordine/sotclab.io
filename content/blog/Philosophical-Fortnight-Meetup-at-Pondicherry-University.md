---
title: "Philosophical Fortnight Meetup at Pondicherry University (6/3/2019)"
image: "images/post/06.png"
date: 2023-09-15
author: "Noordine"
categories: ["foss"]
tags: ["sotc","fshm","meetup","foss"]
draft: false
---

The third public gathering of the year 2019 was a real philosophical
outreach of free software after consecutive technical and awareness
sessions.

## The Pre- gathering events:

Naming the organization discussion happened online, Many suggestions
from various perspectives were suggested. Consecutively, based on inputs
from steering committee the agenda for the meeting on 6/3/2019 was
prepared.

## Event organization:

It all started with the questions prepared and  poster designed by
Noordine based on the agenda of the committee.

<figure class="kg-card kg-image-card kg-width-wide kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-5.png"
class="kg-image" loading="lazy" />
<figcaption>Poster !!!!!</figcaption>
</figure>

Posters were pasted at each department and hostels. Every pillar of
cafeteria and all the public gathering spots around the premises of
Pondicherry University held the posters. Thanks to the great efforts
taken by Prabha and Noordine.

SOTC had invited speakers from FSHM to deliver the talk on the agenda.

Prasanna Vengadesh - Need for Free & Open Source Software, transparency

Kamalavelan - Commons and its Impact

## On the Evening:

Pondicherry University has been place of awesome green vegetation. The
meeting was planned on DDE lawn which has been the hood for various
revolutionary events. It is a spectacular green blanket and public
attention grabbing spot.

<figure class="kg-card kg-image-card kg-width-full kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-6.png"
class="kg-image" loading="lazy" />
<figcaption>Ambience of the lawn</figcaption>
</figure>

A total of about 30 members participated in the meeting. Most of them
where students of PG From Pondicherry University. The meeting started at
4:40 preceded  by Noordine.

Kamal gave an introduction about the Free software organizations and its
role along with the briefing about FSHM and club.

<figure class="kg-card kg-image-card kg-width-full kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-7.png"
class="kg-image" loading="lazy" />
<figcaption>Prasanna on Privacy</figcaption>
</figure>

**Prasanna on privacy**: How privacy is being exploited through all the
gadgets which we use in our day to day life and how free software
mitigates it. He also came up with the black box and the white box(glass
box) analogy to demonstrate the transparency in the code and the need of
the open code to ensure that no hidden bugs may be present to exploit
us.

He also shared the hard copy of the document "Public Money Public Code "
from _fsfe_

<figure class="kg-card kg-image-card kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-9.png"
class="kg-image" loading="lazy" />
<figcaption>Cover of the book</figcaption>
</figure>

> Book is under creative commons license and can be downloaded at :

> [https://flossbcn.org/sites/default/files/2019-01/PMPC-Modernising-with-Free-Software.pdf](https://flossbcn.org/sites/default/files/2019-01/PMPC-Modernising-with-Free-Software.pdf?ref=blog.fshm.org)

We, the public fund public institutions with our taxes but the research
and the intellectual amenities created by such universities are again
being inaccessible by the public themselves.

Charan from Law Department added a brief about the CMRL which acts as a
private entity instead of being a public entity and the parking fare
price fixation problems behind it. He concluded that, one had to enter
that private space by accepting its T&C and people have been pushed to
accept whatever T&C they levy on us.

<figure class="kg-card kg-image-card kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-11.png"
class="kg-image" loading="lazy" />
<figcaption>Casual click</figcaption>
</figure>

## The Cradle of Commons:

Kamal spoke on commons and how free software was built: One of the
world’s best collaborative free project is Linux with contributors from
all around the globe.

### Collaborative projects - case analysis

Kamal also gave an introduction to the collaborative work of the survey
on Free software adoption by Noordine .

A discussion to demonstrate such public projects started with the
question

> How could one fabricate a questionnaire with public participation ?

Various answers was got from the gathering

> Prabha  : Gather a group of volunteers and collect from them  
> Arun   : Give back credit for who have contributed by which we can
> make them interested to contribute  
> Ajith and Vignesh : Make populous to contribute by incentivizing them

Ganesh stated that Questionnaire being made with public participation
gives intellectual incentive to everyone. Collaborating in such open
projects will inherently provide the knowledge of other phases too for
its participants.

Social philosophy in tech includes affordability availability and
accessibility

  
**Affordability**   -  Making the software free such that everyone can
use and tweak it.

**Availability**    - Collaborating such that people at other place
could also see and contribute

**Accessibility** - Using open access curbs the transferring rights of a
paper to the publisher with all the work a researcher has done. By which
the inventions are made public and all the people could access it and
improve the livelihood of humanity

> Even though the meeting was beyond the time fixed many enthusiastic
> volunteers were present.

## Bits of Bitcoin

The discussion continued with the question by Baghya about bitcoin

<figure class="kg-card kg-image-card kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-12.png"
class="kg-image" loading="lazy" />
<figcaption>No, Bitcoin does not look like this as its a cryptocurrency
!!</figcaption>
</figure>

The peer to peer payment nature of the bitcoin was explained in brief.
Along with the questions: is bitcoin Affordable, sustainable and
ethical.

> The bitcoin is being only minable by a person who has the capacity to
> afford for the infrastructure that can mine it.

Bitcoin is prohibited to be used in India. The discussion also covered
the energy being invested in mining the currency. We deplete the energy
and try to mine the bit coin.

## The last cup of talk

Kamal spoke on the Intellectual property rights, the software patents
and some rights reserved

> Some rights reserved -
> [https://creativecommons.org/tag/some-rights-reserved/](https://creativecommons.org/tag/some-rights-reserved/?ref=blog.fshm.org)

He added that software could be not patented in India and also gave the
different licensing mechanisms in creative commons.

### Creative commons licenses

<figure class="kg-card kg-image-card kg-width-full kg-card-hascaption">
<img src="https://blog.fshm.org/content/images/2019/03/image-13.png"
class="kg-image" loading="lazy" />
<figcaption>Click around and upload to Wikimedia under the license you
wish</figcaption>
</figure>

We go to various places and capture spectacular scenarios. Without just
dumping it in our hard drives we can share the ecstasy and joy with the
world such that the whole world could enjoy it and make use of it.

Some of the Wikimedia contributions by Arunekumar(PhD Scholar, Department of Computer Science) on Pondicherry
University vegetation

> [https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_04.jpg](https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_04.jpg?ref=blog.fshm.org)

> [https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_02.jpg](https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_02.jpg?ref=blog.fshm.org)

> [https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_01.jpg](https://commons.wikimedia.org/wiki/File:Pondicherry_university_vegetation_01.jpg?ref=blog.fshm.org)

Some other uploads on Pondicherry

> [https://commons.wikimedia.org/wiki/File:Podicherry_port_04.JPG](https://commons.wikimedia.org/wiki/File:Podicherry_port_04.JPG?ref=blog.fshm.org)

> [https://commons.wikimedia.org/wiki/File:Podicherry_port_03.JPG](https://commons.wikimedia.org/wiki/File:Podicherry_port_03.JPG?ref=blog.fshm.org)

> [https://commons.wikimedia.org/wiki/File:Podicherry_port_01.JPG](https://commons.wikimedia.org/wiki/File:Podicherry_port_01.JPG?ref=blog.fshm.org)

You too can contribute by uploading your clicks to :

> [https://commons.wikimedia.org/wiki/Main_Page](https://commons.wikimedia.org/wiki/Main_Page?ref=blog.fshm.org)

The meeting concluded around 7:00 PM

It was an collaborative effort by all the organizers, speakers and the
audience who spent their valuable time in delivering and understanding
the technology exploitation we suffer. These awareness must propagate
into the public only by which we can progress towards the ideal society
of breathing humanity and peace.

> The first curiosity of a child is to break something and see what is
> inside it. If we start practicing some phenomenon which trims such
> habit on its blossoming stage what would the next generation tweak on
