---
title: "Ken Harris"
image: "images/author/KenHV.png"
email: "yo@kenharris.xyz"
date: 2021-01-01
draft: false
social:
- icon: "la-github"
  link: "https://github.com/KenHV"
- icon: "la-telegram-plane"
  link: "https://telegram.dog/KenHV"
- icon: "lar la-comments"
  link: "https://matrix.to/#/@kenhv:matrix.org"
---

I code, lift, and mope around. I’m currently doing my bachelors in Computer Science.
You can find and use most of my work for free on GitHub.
