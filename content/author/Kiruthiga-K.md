---
title: "Kiruthiga K"
image: "images/author/kiruthiga.jpg"
email: "kiruanime2003@gmail.com"
date: 2021-01-01
draft: false
social:
- icon: "la-gitlab"
  link: "https://gitlab.com/kiruanime2003"
- icon: "la-linkedin-in"
  link: "https://www.linkedin.com/in/kiruthiga-kanagalingam-6a6641200/"
---

I love to design, create, visualize, experiment and analyze.

I am passionate about designing and love animations. I enjoy designing websites, apps. I believe in designing a cryptic user interface paves way for a user-friendly experience.

I believe in the power of visualization and focus. I am drawn towards visually powerful things, which inspires me to create things which leave a visual impact in people's minds.
